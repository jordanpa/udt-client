#include <ccc.h>
#include <common.h>
#include <udt.h>

//#include <vector>
// This is a NewReno implementation
class CTCP : public CCC {
public:
  void init() {
    m_bSlowStart = true;
    m_issthresh = 83333;

    m_dPktSndPeriod = 0.0;
    m_dCWndSize = 2.0;

    setACKInterval(2);
    setRTO(1000000);
  }

  virtual void onACK(int32_t ack) {
    if (ack == m_iLastACK) {
      if (3 == ++m_iDupACKCount)
        DupACKAction();
      else if (m_iDupACKCount > 3)
        m_dCWndSize += 1.0;
      else
        ACKAction();
    } else {
      if (m_iDupACKCount >= 3)
        m_dCWndSize = m_issthresh;

      m_iLastACK = ack;
      m_iDupACKCount = 1;

      ACKAction();
    }
  }

  virtual void onTimeout() {
    m_issthresh = getPerfInfo()->pktFlightSize / 2;
    if (m_issthresh < 2)
      m_issthresh = 2;

    m_bSlowStart = true;
    m_dCWndSize = 2.0;
  }

protected:
  virtual void ACKAction() {
    if (m_bSlowStart) {
      m_dCWndSize += 1.0;

      if (m_dCWndSize >= m_issthresh)
        m_bSlowStart = false;
    } else
      m_dCWndSize += 1.0 / m_dCWndSize;
  }

  virtual void DupACKAction() {
    m_bSlowStart = false;

    m_issthresh = getPerfInfo()->pktFlightSize / 2;
    if (m_issthresh < 2)
      m_issthresh = 2;

    m_dCWndSize = m_issthresh + 3;
  }

protected:
  int m_issthresh;
  bool m_bSlowStart;

  int m_iDupACKCount;
  int m_iLastACK;
};

class RenoTCP : public CCC {

public:
  void init() {
    m_iLastACK = -1;
    m_iDupACKCount = 0;

    m_bSlowStart = true;
    m_issthresh = 83333;

    m_dPktSndPeriod = 0.0;
    m_dCWndSize = 1.0;

    setACKInterval(2);
    // setACKTimer(1000);
    setRTO(1000000);
  }

  virtual void onACK(int32_t ack) {
    if (ack == m_iLastACK) {
      m_iDupACKCount++;
    } else {
      m_iDupACKCount = 0;
    }

    // Slow start phase
    if (m_bSlowStart) {
      // ignore ACKs completely and simply increase
      // window size exponentially
      m_dCWndSize += 1.0;

      // Congestion avoidance phase
      // Duplicate ACK detected, which shouldn't be a false alarm
    } else if (ack == m_iLastACK && m_iDupACKCount >= 3) {
      // 3 duplicate ACKs detected --> do congestion avoidance
      m_dCWndSize /= 2.0;
      m_issthresh = m_dCWndSize;

      // In-order ACK detected (or false alarm), increase cwnd size
      // this will seem like increasing cwnd by 1 in every RTT
    } else {
      m_dCWndSize += 1.0 / m_dCWndSize;
    }

    m_iLastACK = ack;

    // Slow start phase ends normally
    if (m_dCWndSize > m_issthresh) {
      m_bSlowStart = false;
    }
  }

  virtual void onTimeout() {
    // packet loss detected!

    // set threshold to half of cwnd
    // in slow start
    m_issthresh = m_dCWndSize / 2.0;
    // Maybe this should be done instead
    // m_issthresh = getPerfInfo()->pktFlightSize / 2;
    if (m_issthresh < 2)
      m_issthresh = 2;

    // reset cwnd size
    m_dCWndSize = 1.0;

    m_bSlowStart = true;
  }

protected:
  int m_issthresh;
  bool m_bSlowStart;

  int m_iDupACKCount;
  int m_iLastACK;
};

/*
struct VegasTimestamp {
  uint64_t time;
  int32_t pkt_no;
};

class VegasTCP: public CTCP
{
   void init()
   {
      m_bSlowStart = true;
      m_issthresh = 83333;

      m_dPktSndPeriod = 0.0;
      m_dCWndSize = 2.0;

      m_ibaseRTT = 0x7fffffff;

      //m_vTimestamp.resize(m_dCWndSize)

      setACKInterval(2);
      setRTO(1000000);
   }

   virtual void onPktSend(const CPacket* pkt)
   {
     // add new timestamp to vector
     m_vTimestamp.push_back(VegasTimestamp());

     // update its fields
     m_vTimestamp.end->pkt_no = pkt->getAckSeqNo();
     m_vTimestamp.end->time = CTimer::getTime();
   }

   virtual void onACK(const int& ack)
   {
      // get current time
      uint64_t time = CTimer::getTime();
      uint64_t rtt;
      // get package send time
      int32_t seq_no = 0;

      while (seq_no < ack) {
        struct VegasTimestamp ts = m_vTimestamp.pop_front();
        seq_no = ts->pkt_no
        rtt = ts->time;
      }

      // calculate actual rtt (in usec)
      rtt = time - diff;


      if (ack == m_iLastACK)
      {
         if (3 == ++ m_iDupACKCount)
            DupACKAction();
         else if (m_iDupACKCount > 3)
            m_dCWndSize += 1.0;
         else
            ACKAction();
      }
      else
      {
         if (m_iDupACKCount >= 3)
            m_dCWndSize = m_issthresh;

         m_iLastACK = ack;
         m_iDupACKCount = 1;

         ACKAction();
      }
   }

   virtual void onTimeout()
   {
      m_issthresh = getPerfInfo()->pktFlightSize / 2;
      if (m_issthresh < 2)
         m_issthresh = 2;

      m_bSlowStart = true;
      m_dCWndSize = 2.0;
   }


protected:
   virtual void ACKAction()
   {
      if (m_bSlowStart)
      {
         m_dCWndSize += 1.0;

         if (m_dCWndSize >= m_issthresh)
            m_bSlowStart = false;
      }
      else
         m_dCWndSize += 1.0/m_dCWndSize;
   }

   virtual void DupACKAction()
   {
      m_bSlowStart = false;

      m_issthresh = getPerfInfo()->pktFlightSize / 2;
      if (m_issthresh < 2)
         m_issthresh = 2;

      m_dCWndSize = m_issthresh + 3;
   }

protected:
   //Stores the timestamp on a packet send
   std::vector<struct VegasTimestamp> m_vTimestamp;

   uint16_t m_icntRTT; // # of RTTs measured within last RTT
   int32_t m_iminRTT; // min of RTTs measured within last RTT (in usec)
   int32_t m_ibaseRTT; // min of all RTT measurements (in usec)

   uint32_t m_iRTTbeg;
   uint32_t m_iRTTend;
   uint32_t m_iRTTcwnd;

   int m_issthresh;
   bool m_bSlowStart;

   int m_iDupACKCount;
   int m_iLastACK;

   static const int alpha = 2;
   static const int beta  = 4;
   static const int gamma = 1;
};
*/

class CUDPBlast : public CCC {
public:
  CUDPBlast() {
    m_dPktSndPeriod = 1000000;
    m_dCWndSize = 83333.0;
  }

public:
  void setRate(double mbps) { m_dPktSndPeriod = (m_iMSS * 8.0) / mbps; }
};
