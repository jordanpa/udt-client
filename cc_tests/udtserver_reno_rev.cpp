#ifndef WIN32
#include <cstdlib>
#include <cstring>
#include <netdb.h>
#include <sys/time.h>
#include <unistd.h>
#else
#include <winsock2.h>
#include <ws2tcpip.h>
#include <wspiapi.h>
#endif
#include "cc.h"
#include "test_util.h"
#include <chrono>
#include <csignal>
#include <iostream>
#include <thread>
#include <udt.h>

using namespace std;

#ifndef WIN32
void *monitor(void *);
#else
DWORD WINAPI monitor(LPVOID);
#endif

#ifndef WIN32
void *senddata(void *);
#else
DWORD WINAPI senddata(LPVOID);
#endif

UDTSOCKET serv;

void signalHandler(int signum) {
  cout << "Interrupt signal (" << signum << ") received.\n";

  UDT::close(serv);
  exit(signum);
}

int main(int argc, char *argv[]) {
  // register signal SIGINT and signal handler
  signal(SIGTERM, signalHandler);
  if ((1 != argc) && ((2 != argc) || (0 == atoi(argv[1])))) {
    cout << "usage: appserver [server_port]" << endl;
    return 0;
  }

  // Automatically start up and clean up UDT module.
  UDTUpDown _udt_;

  addrinfo hints;
  addrinfo *res;

  memset(&hints, 0, sizeof(struct addrinfo));

  hints.ai_flags = AI_PASSIVE;
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  // hints.ai_socktype = SOCK_DGRAM;

  string service("9000");
  if (2 == argc)
    service = argv[1];

  if (0 != getaddrinfo(NULL, service.c_str(), &hints, &res)) {
    cout << "illegal port number or port is busy.\n" << endl;
    return 0;
  }

  serv = UDT::socket(res->ai_family, res->ai_socktype, res->ai_protocol);

  // UDT Options
  UDT::setsockopt(serv, 0, UDT_CC, new CCCFactory<RenoTCP>,
                  sizeof(CCCFactory<RenoTCP>));
  // UDT::setsockopt(serv, 0, UDT_SNDSYN, &block, sizeof(bool));
  // UDT::setsockopt(serv, 0, UDT_MSS, new int(9000), sizeof(int));
  // UDT::setsockopt(serv, 0, UDT_SNDBUF, new int(10000000), sizeof(int));
  // bool block = true;
  // UDT::setsockopt(serv, 0, UDT_SNDSYN, &block, sizeof(bool));
  //UDT::setsockopt(serv, 0, UDT_SNDTIMEO, new int(3000), sizeof(int));
  // UDT::setsockopt(serv, 0, UDP_RCVBUF, new int(10000000), sizeof(int));

  if (UDT::ERROR == UDT::bind(serv, res->ai_addr, res->ai_addrlen)) {
    cout << "bind: " << UDT::getlasterror().getErrorMessage() << endl;
    return 0;
  }

  freeaddrinfo(res);

  cout << "server is ready at port: " << service << endl;

  if (UDT::ERROR == UDT::listen(serv, 10)) {
    cout << "listen: " << UDT::getlasterror().getErrorMessage() << endl;
    return 0;
  }

  sockaddr_storage clientaddr;
  int addrlen = sizeof(clientaddr);

  UDTSOCKET recver;

  while (true) {
    if (UDT::INVALID_SOCK ==
        (recver = UDT::accept(serv, (sockaddr *)&clientaddr, &addrlen))) {
      cout << "accept: " << UDT::getlasterror().getErrorMessage() << endl;
      return 0;
    }

    UDT::setsockopt(recver, 0, UDT_SNDTIMEO, new int(2000), sizeof(int));
    UDT::setsockopt(recver, 0, UDT_CC, new CCCFactory<RenoTCP>, sizeof(CCCFactory<RenoTCP>));

    char clienthost[NI_MAXHOST];
    char clientservice[NI_MAXSERV];
    getnameinfo((sockaddr *)&clientaddr, addrlen, clienthost,
                sizeof(clienthost), clientservice, sizeof(clientservice),
                NI_NUMERICHOST | NI_NUMERICSERV);
    cout << "new connection: " << clienthost << ":" << clientservice << endl;

    pthread_t sndthread;
    pthread_create(&sndthread, NULL, senddata, &recver);
    pthread_detach(sndthread);

    pthread_t monthread;
    pthread_create(&monthread, NULL, monitor, &recver);
    pthread_detach(monthread);
  }

  UDT::close(serv);

  return 0;
}

long getCurrentTime() {
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return tv.tv_sec * 1000 + tv.tv_usec / 1000;
}

#ifndef WIN32
void *senddata(void *usocket)
#else
DWORD WINAPI senddata(LPVOID usocket)
#endif
{
  UDTSOCKET client = *(UDTSOCKET *)usocket;

  char *data;
  int size = 100000;
  data = new char[size];

  while (true) {
    int ssize = 0;
    int ss;

    while (ssize < size) {
      if (UDT::ERROR ==
          (ss = UDT::send(client, data + ssize, size - ssize, 0))) {
        cout << "send:" << UDT::getlasterror().getErrorMessage() << endl;
        ssize = size - 1;
        break;
        // return 0;
      }

      ssize += ss;
    }

    if (ssize < size) {
      cout << "Early termination!" << endl;
      break;
    }
  }

  cout << "Cleaning up" << endl;
  delete[] data;

  UDT::close(client);

#ifndef WIN32
  return NULL;
#else
  return 0;
#endif
}

#ifndef WIN32
void *monitor(void *s)
#else
DWORD WINAPI monitor(LPVOID s)
#endif
{
  UDTSOCKET u = *(UDTSOCKET *)s;

  UDT::TRACEINFO perf;

  std::chrono::steady_clock::time_point begin =
      std::chrono::steady_clock::now();

  cout << "Time\tSendRate(Mb/"
          "s)\tRTT(ms)\tCWnd\tPktSndPeriod(us)\tRecvACK\tRecvNAK"
       << endl;

  double runtime = 0;
  while (runtime < 10) {
    std::this_thread::sleep_for(std::chrono::milliseconds(100));

    std::chrono::steady_clock::time_point curr =
        std::chrono::steady_clock::now();
    runtime =
        std::chrono::duration_cast<std::chrono::microseconds>(curr - begin)
            .count() /
        1000000.0;

    if (UDT::ERROR == UDT::perfmon(u, &perf)) {
      cout << "perfmon: " << UDT::getlasterror().getErrorMessage() << endl;
      break;
    }

    cout << runtime << "\t" << perf.mbpsSendRate << "\t\t" << perf.msRTT << "\t"
         << perf.pktCongestionWindow << "\t" << perf.usPktSndPeriod << "\t\t\t"
         << perf.pktRecvACK << "\t" << perf.pktRecvNAK << endl;
  }

#ifndef WIN32
  return NULL;
#else
  return 0;
#endif
}
